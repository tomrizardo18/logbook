<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Outgoing extends Model
{
    use HasFactory;

    protected $connection = 'mysql2';
    protected $table = 'outgoing';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'recp_lastname',
        'recp_firstname',
        'recp_midname',
        'recv_ofc',
        'doc_id',
        'doc_name',
        'signature_filename',
       'fullname',
       'date',
       'staff_id',
        'others_recv_ofc'

    ];


    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d h:i:s A', // format date to 12-hour format
    ];

    /**
     * Mutate the date attribute to the specified format.
     *
     * @param  mixed  $value
     * @return string
     */
    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d h:i:s A'); // format date to 12-hour format
    }


    public static function generateCustomId()
    {
        $lastRecord = self::orderBy('id', 'desc')->first();
        $prefixYear = date('Y');
        $prefixMonth = date('m');
        $prefixDay = date('d');
        $nextId = $lastRecord ? intval(substr($lastRecord->id, -4)) + 1 : 1;
        $nextIdPadded = str_pad($nextId, 4, '0', STR_PAD_LEFT);
        return $prefixYear . '-' . $prefixMonth . $prefixDay . '-' . $nextIdPadded ;
    }
}
