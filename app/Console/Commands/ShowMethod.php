<?php

namespace App\Console\Commands;
use App\Http\Controllers\DocDetailController;
use App\Http\Controllers\Home\DashboardController;
use Illuminate\Console\Command;

class ShowMethod extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'show:method';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    $docDetail = new DashboardController();
    print_r($docDetail->creates());
    }
}
