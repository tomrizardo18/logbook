<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Validation\Rules;
use App\Models\User;
use App\Models\DocDetail;
use App\Models\Reports;
use App\Models\Participants;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\Office;
use App\Models\Outgoing;
use App\Models\Incoming;
use Storage;


use DB;
class AppSettings extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');

    // }


 
    
    /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        $alluser = User::orderby('roles')->get();
    
        
        return Inertia::render('User', [
            'users' => $alluser
        ]);
    }
    /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function outgoings()
    {
    
        $offices =Office::orderby('label')->get();
   
  
    
        return Inertia::render('Outgoing', [
         
            'office'=>$offices,
        ]);
    }
    public function incomings()
    {
    
       
   
  
    
        return Inertia::render('Incoming', [
         
            
        ]);
    }


       /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function outgoing(Request $request )
    {
        $start_date = $request['start_date'];
        $end_date =  $request['end_date'];
       
        $allusers = DB::select("CALL get_outgoing(:start_date, :end_date)", [
            'start_date' => $start_date,
            'end_date' => $end_date
        ]);
    
        foreach ($allusers as $user) {
            $filePath = storage_path('app/public/signatures/' . $user->signature_filename);
    
            if (file_exists($filePath)) {
                $fileContent = file_get_contents($filePath);
                $user->signature_data_uri = $fileContent;
            }
        }
    
        return response()->json( [
            'users' => $allusers,
          
        ]);
    }


         /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function incoming(Request $request )
    {
        $start_date = $request['start_date'];
        $end_date =  $request['end_date'];
       
        $allusers = DB::select("CALL get_incoming(:start_date, :end_date)", [
            'start_date' => $start_date,
            'end_date' => $end_date
        ]);
    
        foreach ($allusers as $user) {
            $filePath = storage_path('app/public/signatures/' . $user->signature_filename);
    
            if (file_exists($filePath)) {
                $fileContent = file_get_contents($filePath);
                $user->signature_data_uri = $fileContent;
            }
        }
    
        return response()->json( [
            'users' => $allusers,
          
        ]);
    }
    public function signature()
    {

       
                 $offices =Office::orderby('label')->get();
      

      
        return Inertia::render('Signature',[
    
            'office'=>$offices
             
               
        ]);
    
    }

    public function home()
    {
        // $offices =Office::orderby('label')->get();
       
        // $alluser = Outgoing::orderby('created_at','desc')->get();
    
        // foreach ($alluser as $user) {
        //     $filePath = storage_path('app/public/signatures/' . $user->signature_filename);
    
        //     if (file_exists($filePath)) {
        //         $fileContent = file_get_contents($filePath);
            
        //         $user->signature_data_uri = $fileContent;
        //     }
        // }
    
    

      
        return Inertia::render('Mainmenu',[
    
            // 'users' => $alluser,
            // 'office'=>$offices
               
        ]);
    
    }

        /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function AddUser(Request $request)
    {
        $regx_alpha = '/^[a-zA-Z .ñÑ]+$/';

        // dd($request->all());


        $request->validate([
            // 'mobile' => 'required|numeric', // use this to for testing only
            'email' => 'required|string|email|max:255|unique:user_reports',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
                
            'last_name' => 'required|max:160|regex:'.$regx_alpha,
            'first_name' => 'required|max:160|regex:'.$regx_alpha,
            'mid_name' => 'nullable|max:160|regex:'.$regx_alpha,
            'date' => 'required',
           
            
         

        ], [], []);
        
        

   

        if ($request) {
            $insert_bene = $request->except(['password_confirmation']);
            $mi = !empty($request->mid_name) ? implode('', array_map(function($ar) { return $ar[0]; }, explode(' ', $request->mid_name))) : '';


         $all_caps_exceptionn = ['email'];
         
         $all_caps_exceptionn = ['roles'];
         $all_caps_exceptionn = ['password'];
        
                  array_walk($insert_bene, function(&$arr, $arr_i) use ($all_caps_exceptionn) {
                if (!in_array($arr_i, $all_caps_exceptionn)) {
                    $arr = strtoupper($arr);
                }
            });

        

    
            
       
            $user_profile = User::create($insert_bene);

        }
        
        // event(new Registered($user));
    
        return Inertia::render('User',[
    
            'message'=>"Added Successfully"
        
         
           
    ]);
   
      
      
    
}
        /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function RoleChange(Request $req)
    {
      
        $update_info['roles']=$req['roles'];
        

     
        $save_bene = User::where(['id'=>$req->id])->update($update_info);
        $message = 'Roles Change Successfuly';
        return response()->json([
          
            
            'message'=>$message
        ]);
      
      
    
}
public function addsignature(Request $request)
{
    $regx_alpha = '/^[a-zA-Z .ñÑ]+$/';

    $request->validate([
    
    
        'recp_lastname' => 'required|max:160|regex:'.$regx_alpha,
        'recp_firstname' => 'required|max:160|regex:'.$regx_alpha,
        'recp_midname' => 'nullable|max:160|regex:'.$regx_alpha,
        'doc_name' => 'required|max:160',
        'recv_ofc' => 'required|max:160|regex:'.$regx_alpha,
        'signature_filename'=> 'required',
        'date'=>'required',
        'others_recv_ofc'=>'required_if:recv_ofc,OTHERS'
    ], [
        'recp_lastname.required' => 'Recipient Last name is required',
        'recp_firstname.required' => 'Recipient First name is required',
        'doc_name.required' => 'Document name is required',
        'recv_ofc.required' => 'Receiving office is required',
        'signature_filename.required' => 'Signature is required',
        'date.required' => 'Recipient Date is required'
    ]);

    if ($request->hasFile('signature_filename')) {
        $path = $request->file('signature_filename')->store('public/signatures');
        $insert_bene = $request->except(['password_confirmation']);
        $insert_bene['signature_filename'] = basename($path);
        $insert_bene['staff_id'] = auth()->user()->id;
        $insert_bene['fullname'] = $insert_bene['recp_lastname'] . ', ' . $insert_bene['recp_firstname'];
if (!empty($insert_bene['recp_midname'])) {
    $insert_bene['fullname'] .= ' ' . $insert_bene['recp_midname'];
}
$insert_bene['doc_id'] = Outgoing::generateCustomId(); // generate custom ID
$insert_bene['date'] = $request->input('date');
// $insert_bene['date'] = \Carbon\Carbon::parse($request->input('date'))->format('Y-m-d H:i:s');
$insert_bene['date'] = \Carbon\Carbon::parse($request->input('date'))->format('Y-m-d');
        $all_caps_exceptionn = ['created_at'];
        array_walk($insert_bene, function(&$arr, $arr_i) use ($all_caps_exceptionn) {
            if (!in_array($arr_i, $all_caps_exceptionn)) {
                $arr = strtoupper($arr);
            }
        });

        $user_profile = Outgoing::create($insert_bene);

        return Inertia::render('Outgoing', [
            'message' => "Added Successfully",
         
        ]);
    } else {
        return response()->json(['error' => 'Signature file not found.'], 400);
    }
}

public function addincoming(Request $request)
{
    $regx_alpha = '/^[a-zA-Z .ñÑ]+$/';

    $request->validate([
    
    

        'doc_name' => 'required|max:160',
        'signature_filename'=> 'required',
        'date'=>'required',
       
    ], [

        'signature_filename.required' => 'Signature is required',
        'date.required' => 'Recipient Date is required'
    ]);

    if ($request->hasFile('signature_filename')) {
        $path = $request->file('signature_filename')->store('public/signatures');
        $insert_bene = $request->except(['password_confirmation']);
        $insert_bene['signature_filename'] = basename($path);
        $insert_bene['recp_firstname'] = auth()->user()->first_name;
        $insert_bene['recp_lastname'] = auth()->user()->last_name;
        $insert_bene['recp_midname'] = auth()->user()->mid_name;
        $insert_bene['staff_id'] = auth()->user()->id;
        $insert_bene['fullname'] = $insert_bene['recp_lastname'] . ', ' . $insert_bene['recp_firstname'];
if (!empty($insert_bene['recp_midname'])) {
    $insert_bene['fullname'] .= ' ' . $insert_bene['recp_midname'];
}
$insert_bene['date'] = $request->input('date');
$insert_bene['doc_id'] = Incoming::generateCustomId(); // generate custom ID
// $insert_bene['date'] = \Carbon\Carbon::parse($request->input('date'))->format('Y-m-d H:i:s');
$insert_bene['date'] = \Carbon\Carbon::parse($request->input('date'))->format('Y-m-d');
        $all_caps_exceptionn = ['created_at'];
        array_walk($insert_bene, function(&$arr, $arr_i) use ($all_caps_exceptionn) {
            if (!in_array($arr_i, $all_caps_exceptionn)) {
                $arr = strtoupper($arr);
            }
        });
    

        $user_profile = Incoming::create($insert_bene);

        return Inertia::render('Incoming', [
            'message' => "Added Successfully",
         
        ]);
    } else {
        return response()->json(['error' => 'Signature file not found.'], 400);
    }
}

// public function addsignature(Request $request)
// {
//     $regx_alpha = '/^[a-zA-Z .ñÑ]+$/';

//     // dd($request->all());


//     $request->validate([
//         // 'mobile' => 'required|numeric', // use this to for testing only
//         'email' => 'required|string|email|max:255|unique:user_reports',
//         'password' => ['required', 'confirmed', Rules\Password::defaults()],
            
//         'last_name' => 'required|max:160|regex:'.$regx_alpha,
//         'first_name' => 'required|max:160|regex:'.$regx_alpha,
//         'mid_name' => 'nullable|max:160|regex:'.$regx_alpha,
//         'mid_name' => 'nullable|max:160|regex:'.$regx_alpha,
//         'signature_filename'=> 'required'
       
        
     

//     ], [], []);
    
    


//     if ($request) {
//         $insert_bene = $request->except(['password_confirmation']);
//         $mi = !empty($request->mid_name) ? implode('', array_map(function($ar) { return $ar[0]; }, explode(' ', $request->mid_name))) : '';

//     $user_last = $request['last_name'];
//     $user_first = $request['first_name'];
//     // if (!empty($request->file('signature_filename'))) {
//     //     $insert_bene['signature_filename'] = $this->uploadAttachment($request->signature_filename, $insert_bene['last_name'], $insert_bene['first_name']);
//     // }
//     $all_caps_exceptionn = ['email', 'roles', 'password'];
    
//               array_walk($insert_bene, function(&$arr, $arr_i) use ($all_caps_exceptionn) {
//             if (!in_array($arr_i, $all_caps_exceptionn)) {
//                 $arr = strtoupper($arr);
//             }
//         });

    


//         $ste = $request['signature_filename'];
   
//         $user_profile = User::create($insert_bene);

//     }
    
//     // event(new Registered($user));

//     return Inertia::render('Signature',[

//         'message'=>"Added Successfully",
//         'signature_filename' => $ste
     
       
// ]);

  
  

// }

// public function uploadAttachment(Object $request_file,String $user_last,String $user_first)
//     {
       
//         $folder = 'receipt_files/'.$user_last.'/'.$user_first.'/';

//         $full_path = storage_path('app/'.$folder);
//         $up_filename ="sign"; // sanitize illegal string and replace it wtith dash(-)
//         $image = base64_decode($request_file);

//         // $up_filename = preg_replace('/[^a-zA-Z0-9\-\._ ]/', '-', $request_file->getClientOriginalName().'_'.$refno); // filename with ref number include refno on param first

//         if (!file_exists($full_path)) {
//             Storage::makeDirectory($folder);

//         }

//         if (file_exists($full_path.'/'.$up_filename)) {
//             $filename_without_ext = Str::beforeLast($up_filename, '.');
//             $new_filename = $filename_without_ext.'(1)';
//                 $testfilename = 'membership';
//             $up_filename = $new_filename.' '.$image->clientExtension();
         
//         }

      

//         $uploaded = Storage::putFileAs($folder, $image, $up_filename);
        
    
//             if ($uploaded) {
//             $update_data = [
//                 'signature_filename' => $up_filename,
               
//             ];
            
//             // $return_data = (new Participants)->where('user_id',)->update($update_data);

//         }

//         return $update_data;
//     }

 
// 
public function uploadAttachment($signature_filename, $user_last, $user_first)
{
    $folder = 'receipt_files/'.$user_last.'/'.$user_first.'/';
    $full_path = storage_path('app/'.$folder);
    $up_filename = 'signature.png';

    if (!file_exists($full_path)) {
        Storage::makeDirectory($folder);
    }

    $image = file_get_contents($signature_filename);
    $uploaded = Storage::put($folder.$up_filename, $image);

    if ($uploaded) {
        return $folder.$up_filename;
    }
}
}