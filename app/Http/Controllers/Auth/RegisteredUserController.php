<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Inertia\Inertia;



// use App\Models\Settings\CivilStatus;

class RegisteredUserController extends Controller
{

    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return Inertia::render('Auth/Register', [
            
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $regx_alpha = '/^[a-zA-Z .ñÑ]+$/';

        // dd($request->all());


        $request->validate([
            // 'mobile' => 'required|numeric', // use this to for testing only
            'email' => 'required|string|email|max:255|unique:user_reports',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],

            'last_name' => 'required|max:160|regex:'.$regx_alpha,
            'first_name' => 'required|max:160|regex:'.$regx_alpha,
            'mid_name' => 'nullable|max:160|regex:'.$regx_alpha,
            
           
            
         

        ], [], []);


        $find_info = User::select('id')->where(
            [
                'last_name' => $request->last_name,
                'first_name' => $request->first_name,   
            ]
        )->first();

        if (!empty($find_info)) {
            // return response()->json(['fullname' => "You're already registered."], 422);
            return redirect()->back()->withErrors([
                'fullname' => "You're already registered.",
            ]);
        }

    

        $user = User::create([
        
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'last_name'=>$request->last_name,
            'first_name'=>$request->first_name,
            'mid_name'=>$request->mid_name,
        ]);

        if ($user) {
              
            $mi = !empty($request->mid_name) ? implode('', array_map(function($ar) { return $ar[0]; }, explode(' ', $request->mid_name))) : '';


         $all_caps_exceptionn = ['email'];

        
            

        

    
            
       

        }

        event(new Registered($user));

        // return response()->json(['error' => false, 'message' => "Congratulations! You're now registered. Please check on your mobile number for one time verification code. Thank you.", 'ref_id' => $otp_['ref_id'] ]);

        
        // return inertia('Auth/Register', [
       
        //     // 'sent_otp' => true,
        //     // 'message' => "TEST",
        //     // 'ref_id' => '112021222',
        // ]);
        

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
