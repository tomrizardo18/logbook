<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


use App\Http\Controllers\Home\AppSettings;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Auth/Login', [
        'canRegister' => Route::has('register'),
    ]);
}) ->name('guest_home');

Route::get('/SuccessPage', function () {
    return Inertia::render('SucessPage', [
        'canRegister' => Route::has('register'),
   
    ]);
}) ->name('SuccessPage');

Route::get('/Aboutus', function () {
    return Inertia::render('AboutUs', [
        'canRegister' => Route::has('register'),
   
    ]);
}) ->name('aboutus');





Route::get('/Resources', function () {
    return Inertia::render('Resources', [
        'canRegister' => Route::has('register'),
   
    ]);
}) ->name('faq');

Route::get('/checkstatus', function () {
    return Inertia::render('Checkstatus');
})->name('checkstatus');

Route::get('/Ref', function () {
    return Inertia::render('Ref');
})->name('ref');
Route::get('/SupportForm', function () {
    return Inertia::render('SupportForm');
})->name('supportform');


Route::post('users', [AppSettings::class, 'RoleChange'])->name('rolechange');
Route::post('outgoings', [AppSettings::class, 'outgoing'])->name('outgoing');
Route::post('incomings', [AppSettings::class, 'incoming'])->name('incoming');
Route::post('users.add', [AppSettings::class, 'AddUser'])->name('adduser');
Route::post('signature', [AppSettings::class, 'addsignature'])->name('addsigs');
Route::post('incoming', [AppSettings::class, 'addincoming'])->name('addincoming');






require __DIR__.'/auth.php';
