<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Support\Facades\Http;


class PayRoroTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {

        // $private_rsa_key = "-----BEGIN RSA PRIVATE KEY-----\nMIICWgIBAAKBgGNWfHumns4Sbi2XBAzudLyA49TDuBWJJTEDgcMt3g3LMb/L96AqSBCgTK60xrkHlHUnGfcjkBjA1tuqLTUlNUzZYR/ta8sGqFlN8o8RIHRLv5pcQAcw2COT5vE25M+EI10wEMgiROYQwLzBr2TlZ40GZUlcGWNrsSkMmj3mkzZZAgMBAAECgYAasaPz6ltkTumSkLFC9FUkTpJqm0l9aAQ5fpLBKzMvi7nLq3jYbmQ9K4ZPsD5yfEn1jVNZ6RGQxUk3cCyDyoLe1FxSdaCEJFIYsUwvAsT3C1E9b069Sde6RWgbU87VhQMf5U/SKwxZJvj8rcMiHBObdNjH071mbt1EPhfvGyjl8QJBALXmWBEmtC3wvojMWsoCvZ18bvpyLtH1nOAuX6W/NtG5xBtHaDq/iCE9O4wtLS8tlppS6m6L/2QsPDbOE+nVDG0CQQCLzg/q/t4OKfn9/P02921xP8WEiyEO/iTEwInVuiOM8WIvfnntJ1BPc6LV1UBQdh7xZVlhy92UIkefw2VLV0YdAkAH5i7g38tnS1Ma4bT3nNLbKfIv0hGScJYVbi4dJoac6PJJNPqjwnmI5/3UAoo5yEMIBJ3FmzLGCN28i4p11xipAkAQPyIabQPhIX+rxHqoqJhPUNnjTUfKNj6PntBlxfvbpTGD027X4Hm0wftqXh3bO97crqp0cvyM+m7YYhyjZMEBAkBDcI9weCDxEtlvAnrrh40rwfZu1AyqYRilEVUANpdOLjDciz1vrdPo+IEWFcc32qLgQBVLRjFtdMtr0spAknFm\n-----END RSA PRIVATE KEY-----";

        $private_rsa_key = "-----BEGIN RSA PRIVATE KEY-----\nMIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIrmBgG9AqZEcaZ4+1YAEab0WWyRO8PESbXJFD2ZeP5+3NkIKIYTBE+Xd3BiwtdQE2ESgL7vL8TcaJhASY6K86C/1ydS1gdsBNGI2BYLtWCgSDm1fO8A87DoS5JPoMS45zHV8lsmPG14Y5HyQ/5qyrQSDBjYaBrVS0j1ywdCWR+vAgMBAAECgYBgv4I1mcMLryb+jsx6hcfF4/OZ1LutS78P+US493q4wgLwxWb8pKhfDCDszk3rDeLBvdKhrBc451GPo0/ZEt5I9g5lig8H6MzRqy+e47hwVxY9yxSvguVMqQTSePz1QVlIxMK+4M7PDlbJeFwOFvGxJUOVoh3iOhDtbK70Hs3TCQJBAMRe4rryluIZrNkX+FRSbtn8Kclej3J3IAM5x082fU30wN+dCkdEGoRzi6Ry1NPukYWyYdINlmbhRserylNH4gUCQQC1E3jLtAeY1lzt4UUsIdc5LIX/Rd390s2G3QwBT11h8uI1ymV14h9oqeAHSxso7tTgqDu2RLBjHPOkCh0e+KUjAkA1t98+VO6Bm5R1LuF377TzXM/xjjSfk4bmWv/y420TzkiU41jUdgTrBtDEg8VE1JZqhInN9HmYWzgJly0Z6yghAkABnnz0BVYUDcai8eK2dE8CWd2Q3MURYfMog4157YW+PHe9V2P02+LAAtTX+9nwUcJhh9+Qz6hsGnQrQVPlVP/VAkEAnThDO42QPXvjtm6XVmn5gxXcZklYX2nLZdVyl2M03vf0tbY84XaT4W1J4kEAbYmx91M1I05DDwsDvh4W+WE0rw==\n-----END RSA PRIVATE KEY-----";

        $form_data = [
            'description' => 'Payment Test '.date('m-d-y'),
            'email' => 'testmail@gmail.com',
            'merchantNo' => 'NO120910918674',
            // 'merchantNo' => 'TEST090614781073',
            // 'merchantOrderNo' => '9906230',
            'merchantOrderNo' => rand(0, 999999999),
            'method' => 'gcash',
            // 'method' => 'ECPY',
            'mobile' => '09064675697',
            'name' => 'Test test',
            'payAmount' => '200',
            // 'notifyUrl' => 'https://app.bploparanaque.com/sandbox/bplsbeta_v1/test-hook/payloro/response',
            // 'notifyUrl' => 'https://app.bploparanaque.com/sandbox/webhooks-api-tester/payloro/response.php',
            // 'redirectUrl' => 'https://app.bploparanaque.com/bplsbeta/',
            
        ];

        // {
        //     "payAmount": "200",
        //     "method": "ussc",
        //     "name": "test name",
        //     "sign": "todo",
        //     "description": "description",
        //     "merchantOrderNo": "1639920028703",
        //     "email": "123@123.com",
        //     "merchantNo": "NO120910918674"
        //     }

        ksort($form_data);

        $concat_form_data = implode($form_data);
		
        $ssl_priv_key = openssl_pkey_get_private($private_rsa_key);

        $enc_content = '';

        // foreach($form_data as $fd) {
        foreach (str_split($concat_form_data, 117) as $fd) {
            openssl_private_encrypt($fd, $ncrpyt, $ssl_priv_key);
            $enc_content .= $ncrpyt;
        }

        $api_signature = base64_encode($enc_content);
        
        $form_data['sign'] = $api_signature;

        //api request

        $post = Http::post("https://testgateway.payloro.ph/api/pay/code", $form_data);
        // $post = Http::post("https://api.payloro.tech/api/pay/query", $form_data);
        $response = $post->json();

        dd($response);

    }
}
